<?php

$name = '';
$surname = '';
$email = '';
$tel = '';
$subject = '';
$payment = '';
$mailing = false;

if ($_POST)
{

    if (isset($_POST['name']))
    {
        $name = trim($_POST['name']);
    }

    if (isset($_POST['surname']))
    {
        $surname = trim($_POST['surname']);
    }

    if (isset($_POST['email']))
    {
        $email = trim($_POST['email']);
    }

    if (isset($_POST['tel']))
    {
        $tel = trim($_POST['tel']);
    }

    if (isset($_POST['subject']))
    {
        $subject = trim($_POST['subject']);
    }

    if (isset($_POST['payment']))
    {
        $payment = trim($_POST['payment']);
    }

    if (isset($_POST['mailing']))
    {
        $mailing = true;
    }

    // проверим данные
    
    $errors = [];

    if (empty($name))
    {
        $errors['name'] = 'Не введено имя';
    }

    if (empty($surname))
    {
        $errors['surname'] = 'Не введена фамилия';
    }

    if (empty($email))
    {
        $errors['email'] = 'Не введен E-mail';
    }

    if (empty($tel))
    {
        $errors['tel'] = 'Не введен телефон';
    }

    if (empty($subject))
    {
        $errors['subject'] = 'Не выбрана тема';
    }

    if (empty($payment))
    {
        $errors['payment'] = 'Не выбран метод оплаты';
    }

    if (empty($errors))
    {
        $contents = '';
        $contents .= 'Имя: ' . $name . "\n";
        $contents .= 'Фамилия: ' . $surname . "\n";
        $contents .= 'E-mail: ' . $email . "\n";
        $contents .= 'Телефон: ' . $tel . "\n";
        $contents .= 'Тематика: ' . $subject . "\n";
        $contents .= 'Метод оплаты: ' . $payment . "\n";
        $contents .= 'Подписка на рассылку: ' . ($mailing ? 'да' : 'нет') . "\n";

        $filename = date('Y-m-d-H-i-s') . '.txt';

        file_put_contents($filename, $contents);

        $message = 'Ваша заявка принята';
    }

}

?><html>
<head>
<meta charset="utf-8">
<link href="http://cdn.forlabs.ru/bootstrap/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <?php
    if (isset($message))
    {
        echo '<p>' . $message . '</p>';
    }
    else
    {
        ?>
        <?php if (!empty($errors)) { ?>
        <div class="alert alert-danger">
        <p>У вас есть ошибки при заполнении формы:</p>
        <ul>
            <?php foreach ($errors as $error) { ?>
            <li><?= $error ?></li>
            <?php } ?>
        </ul>
        </div>
        <?php } ?>
        <form method="POST" action="">

            <label>Имя:</label>
            <input type="text" name="name" class="form-control" value="<?= $name ?>">
            <br>

            <label>Фамилия:</label>
            <input type="text" name="surname" class="form-control" value="<?= $surname ?>">
            <br>

            <label>Электронный адрес:</label>
            <input type="email" name="email" class="form-control" value="<?= $email ?>">
            <br>

            <label>Телефон:</label>
            <input type="text" name="tel" class="form-control" value="<?= $tel ?>">
            <br>


            <label>Тематика конференции:</label>
            <select class="form-control" name="subject">
                <option<?php if ($subject === 'Бизнес') { echo ' selected'; } ?>>Бизнес</option>
                <option<?php if ($subject === 'Технологии') { echo ' selected'; } ?>>Технологии</option>
                <option<?php if ($subject === 'Реклама') { echo ' selected'; } ?>>Реклама</option>
                <option<?php if ($subject === 'Маркетинг') { echo ' selected'; } ?>>Маркетинг</option>
            </select>
            <br>

            <label>Метод оплаты:</label>
            <select class="form-control" name="payment">
                <option<?= $payment === 'WebMoney' ? ' selected' : '' ?>>WebMoney</option>
                <option<?= $payment === 'Яндекс.Деньги' ? ' selected' : '' ?>>Яндекс.Деньги</option>
                <option<?= $payment === 'PayPal' ? ' selected' : '' ?>>PayPal</option>
                <option<?= $payment === 'кредитная карта' ? ' selected' : '' ?>>кредитная карта</option>
            </select>
            <br>

            <label>
                <input type="checkbox" name="mailing">
                Хочу получать рассылку о конференции
            </label>
            
            <br>
            
            <button type="submit" class="btn btn-primary">
                Отправить форму
            </button>
        </form>
        <?php
    }
    ?>

</div>
</body>
</html>